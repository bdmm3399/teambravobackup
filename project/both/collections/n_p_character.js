
// A non playable character in the game

NPCharacter = new Mongo.Collection('n_p_character');

NPCharacter.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  objectCode:{
    type:String
  },
  npCharacterRequestCodes:{
    type:[String],
    optional: true
  },
  scenarioCodes:{
    type:[String],
    optional: true
  }

}));